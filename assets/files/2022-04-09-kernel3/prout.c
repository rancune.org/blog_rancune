#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>

MODULE_DESCRIPTION("Prout Prout");
MODULE_AUTHOR("CFS/LFS");
MODULE_LICENSE("PPL");

#define DEVNAME "prout"

static char proutdev[] = "proutproutproutprout\n" ;
static int proutlen ;
static int major ;

static ssize_t prout_read( struct file *, char*, size_t, loff_t * ); 

static struct file_operations fops = {
    .read = prout_read
};

static int 
prout_init(void) 
{
    printk("coucou la voila\n") ;
    major=register_chrdev(0, DEVNAME, &fops) ;
    if (major<0) {
        printk("nacasse!!\n");
        return major ;
    }
    proutlen = strlen(proutdev) ;
    return 0 ;
}


static void
prout_exit(void)
{
    if (major != 0 ) 
        unregister_chrdev(major, DEVNAME ) ;
    printk("napuuuuuuuuuuuuuuuu\n") ;   
}

static ssize_t
prout_read( struct file *filep, char* buf, size_t len, loff_t *off ) 
{
    int minlen = min( proutlen, len ) ;
    if ( copy_to_user( buf, proutdev ,minlen ) != 0 ) {
        printk("nacasse\n");
        return -EFAULT;
    }
    return minlen ;
}

module_init(prout_init);
module_exit(prout_exit);
