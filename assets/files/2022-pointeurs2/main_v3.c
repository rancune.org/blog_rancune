#include <stdio.h>

void dis_moi_des_mots_doux( int ) ;
void remise_a_zero(int*) ;

int 
main() 
{
    int A ;
    A = 69 ;
    dis_moi_des_mots_doux( A ) ;    
    remise_a_zero( &A ) ;           
    dis_moi_des_mots_doux( A ) ;  
    return 0 ;
}

void 
dis_moi_des_mots_doux( int n ) 
{
   printf("Oh ... un %d ! Comme c'est gentil !!!\n", n ) ;
}

void
remise_a_zero( int* k )
{
    *k = 0 ;
}
