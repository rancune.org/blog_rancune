#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

int main() {

    int fd, rc ;
    fd = open("/dev/prout", O_RDONLY) ;
    if ( fd <0 ) {

        return EXIT_FAILURE ;

    }
    rc = close(fd) ;
    printf("Errno vaut %d \n", errno) ;

    return rc ;
}
