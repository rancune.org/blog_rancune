#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>

MODULE_DESCRIPTION("Prout Prout");
MODULE_AUTHOR("CFS/LFS");
MODULE_LICENSE("PPL");

#define DEVNAME "prout"


static int
prout_open( struct inode *, struct file *) ;

static int 
prout_close( struct inode *, struct file *) ; 

static struct file_operations fops = {
    .open = prout_open,
    .release = prout_close
};

static int major ;


static int 
prout_init(void) 
{
    printk("coucou le voila\n") ;
    major=register_chrdev(0, DEVNAME, &fops) ;
    if (major<0) {
        printk("nacasse!!\n");
        return major ;
    }
    printk("Major: %d\n", major) ;
    return 0 ;
}


static void
prout_exit(void)
{
    if (major != 0 ) {
        unregister_chrdev(major, DEVNAME ) ;
    }
    printk("napuuuuuuuuuuuuuuuu\n") ;   
}


static int
prout_open( struct inode *in, struct file *filep )
{
    printk("Owiiii ! ouvre moi !\n" ) ;
    return 0 ;
}

static int 
prout_close( struct inode *in, struct file *filep ) 
{ 
    printk("Mmmh! ferme bien la porte !\n" ) ; 
    return 0 ; 
}

module_init(prout_init);
module_exit(prout_exit);
