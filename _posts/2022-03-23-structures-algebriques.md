---
layout: post
category: [sciences]
title: "Structures algébriques : THE poster" 
author: rancune
except: Un premier post, parce qu'il faut bien commencer par quelquechose ! 
---

Les structures algébriques ne sont pas très compliquées à comprendre, mais mazette ! Quel vocabulaire !
J'ai dû, pour le boulot, me replonger un peu dans cette jungle de définitions et j'en ai sorti ce petit poster :


![THE poster de toutes les structures](/assets/img/2022_algebre/poster_algebre.png)

Je vous le laisse en format png et svg, pour votre usage personnel : 

  - [PNG](/assets/img/2022_algebre/poster_algebre.png){:target="_blank"}{:rel="noopener noreferrer"} Parce que le fond transparent, c'est pratique !
  
  - [SVG](/assets/img/2022_algebre/poster_algebre.svg){:target="_blank"}{:rel="noopener noreferrer"} Comme ça vous pourrez en faire un poster :)

N'hésitez pas à me faire remonter les éventuelles erreurs ou compléments !

Rancune.

