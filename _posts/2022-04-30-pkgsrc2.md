---
layout: post
category: [sysadmin, outils, imil]
title: "pkgsrc (2/6)" 
author: rancune
prevPart: _posts/2022-04-25-pkgsrc.md
nextPart: _posts/2022-05-08-pkgsrc3.md
---

Et nous voici repartis pour une nouvelle prise de notes sur les streams d'[Imil](https://twitter.com/iMilnb) ! Nous continuons, dans cette [vidéo](https://www.youtube.com/watch?v=ilR6ThLAqJ4) à découvrir le système pkgsrc et la vie de mainteneur de package.

# Previously on Imil TV ...

Commençons par un petit rappel de ce que nous avions vu la dernière fois. Intéressés par un soft disponible sur gitlab, [truefalse v1.0](mettre le bon lien github), nous nous sommes dit que nous allions le **packager** avec pkgsrc ...

Pkgsrc, c'est un système de gestion de packages made in NetBSD et basé sur bmake. Il fonctionne sur de nombreuses autres plateformes, parmi lesquels on notera Mac OSX ou même windows ! Les commandes présentées ici sont d'ailleurs testées sous gentoo par votre serviteur :) 

Nous avons commencé notre épopée dans le monde des mainteneurs de package en utilisant un outil appelé **url2pkg**, permettant de transformer une url en package :

{% highlight term %}
$ mkdir ~/pkgsrc/wip/truefalse
$ cd ~/pkgsrc/wip/truefalse
$ url2pkg https://gitlab.com/iMil/truefalse/-/archive/v1.0/truefalse-v1.0.tar.gz
{% endhighlight %}

On obtient un Makefile, que l'on modifie un peu :

{% highlight Makefile %}
PROGNAME=	truefalse
DISTNAME=	truefalse-v1.0
PKGNAME=	${DISTNAME:S,-v,-,}
CATEGORIES=	misc
MASTER_SITES=	https://gitlab.com/iMil/truefalse/-/archive/v1.0/

MAINTAINER=	truefalse@rancune.org
HOMEPAGE=	https://gitlab.com/iMil/truefalse/-/archive/v1.0/
COMMENT=	First attempt at making a package
LICENSE=	original-bsd

INSTALLATION_DIRS=	bin

do-install:
	${INSTALL_PROGRAM} ${WRKSRC}/${PROGNAME} ${DESTDIR}/${PREFIX}/bin/${PROGNAME}

.include "../../mk/bsd.pkg.mk"
{% endhighlight %}

Habituellement pour compiler un package, on utilise la commande ./configure, qui génère un Makefile possédant une target install. On lance alors un simple :

{% highlight term %}
make install
{% endhighlight %}

Mais dans le projet que nous sommes en train de packager, le Makefile n'est pas super évolué . C'est un Makefile bmake très simple, qui ne comporte pas de cible d'installation.

C'est pour cette raison que nous avons surchargé la target do-install dans notre package. D'où ces lignes en particulier :

{% highlight Makefile %}
do-install:
	${INSTALL_PROGRAM} ${WRKSRC}/${PROGNAME} ${DESTDIR}/${PREFIX}/bin/${PROGNAME}
{% endhighlight %}

A ce stade, un petit rappel est peut-être nécessaire concernant les variables DESTDIR et PREFIX. Le PREFIX, vous en avez probablement l'habitude si vous avec déjà manipulé les autotools et le célèbre script "configure". En effet, par défaut, ce script configure utilise un prefix qui vaut "/usr/local" pour effectuer les installations. Cela permet de choisir le chemin de base de l'installation : les binaires iront dans /usr/local/bin, les librairies dans /usr/local/lib et ainsi de suite ... Si on veut déployer le soft ailleurs, dans /usr par exemple, il suffit de modifier la variable PREFIX.

Pkgsrc introduit une seconde variable, DESTDIR, qui va nous permettre de "simuler" une installation dans un répertoire grâce à la cible stage-install. Si on tape :

{% highlight term %}
bmake stage-install
{% endhighlight %}

L'installation ne va pas se faire de façon habituelle mais localement, dans le sous-répertoire work. C'est très pratique pour pouvoir vérifier les destinations d'installation de notre package.

Lorsque nous tapons "bmake install", DESTDIR vaut "/". 

Lorsque nous tapons "bmake stage-install", DESTDIR vaut "/home/rancune/pkgsrc/wip/truefalse/work". 

Tout simplement !


# pkgsrc et ses targets

Un petit mot sur les différentes cibles proposées par pkg-src. Il en existe un certain nombre et elles sont plutôt pratiques ! Le lecteur intéressé pourra se rendre sur :

[https://wiki.netbsd.org/pkgsrc/intro_to_packaging/](https://wiki.netbsd.org/pkgsrc/intro_to_packaging/)

En voici une petite sélection :

  * **bmake stage-install** 
 
  Permet une simulation de l'installation dans un répertoire local au package, comme vu ci-dessus.
  
  * **bmake print-PLIST**

  Liste des fichiers du paquet installés sur le filesystem 

  * **bmake clean** 

  Efface le fichier source du répertoire de travail, pour repartir d'un projet tout propre !
  
  * **bmake distclean**

  Efface l'archive source téléchargée

  * **bmake fetch**

  Récupère le fichier, et vérifie si son checksum correspond à celui enregistré

  * **bmake extract**

  Extrait (décompresse) les fichiers sources de l'archive et les place dans le répertoire de travail

  * **bmake patch**

  Applique les patchs (du package) aux sources

  * **bmake show-depends**

  Permet de visualiser les dépendances du package, s'il y en a :

  {% highlight term %}
  $ cd /home/rancune/pkgsrc/x11/gnome-terminal

  $ bmake show-depends

  dconf>=0.36.0nb6:../../devel/dconf
  glib2>=2.70.2:../../devel/glib2
  libuuid>=2.18:../../devel/libuuid
  hicolor-icon-theme>=0.9nb1:../../graphics/hicolor-icon-theme
  desktop-file-utils>=0.10nb1:../../sysutils/desktop-file-utils
  gsettings-desktop-schemas>=3.4.2:../../sysutils/gsettings-desktop-schemas
  vte3>=0.60.3nb12:../../x11/vte3
  {% endhighlight %}

  * **bmake show-options**

  Les packages peuvent parfois avoir des options, c'est à dire des fonctionnalités que l'on peut choisir de compiler ou pas lors de l'installation dudit package. Pour consulter ces options, on procède de la façon suivante :

{% highlight term %}
$ cd /home/rancune/pkgsrc/multimedia/vlc

$ bmake show-options
Any of the following general options may be selected:
        alsa     Enable ALSA support.
        avahi    Enable DNS service discovery and multicast DNS support.
        dbus     Enable dbus (desktop bus) support.
        debug    Enable debugging facilities in the package.
        dts      Enable DTS Coherent Acoustics support.
        jack     Enable support for the JACK audio server.
        lirc     Enable LIRC receiving and sending IR signals support.
        pulseaudio       Enable support for the PulseAudio sound server.
        qt5      Enable support for QT5.
        vaapi    Enable support for VAAPI.
        vdpau    Enable support for VDPAU.
        vlc-skins        Enable skins support in VLC.
        x11      Enable X11 support.

These options are enabled by default:
        alsa dbus lirc qt5 vaapi vdpau x11

These options are currently enabled:
        alsa dbus lirc qt5 vaapi vdpau x11

You can select which build options to use by setting PKG_DEFAULT_OPTIONS
or PKG_OPTIONS.vlc.
{% endhighlight %}

Comme indiqué par le message affiché, la selection des options se fera par la variable PKG_DEFAULT_OPTIONS pour des choix généraux, et par la variable PKG_OPTIONS.\<package\> pour un package en particulier.

Ceci se fait dans le fichier mk.conf, qui contient la configuration de pkgsrc.

  * **make show-var VARNAME=\<variable\>**

Permet de consulter la valeur d'une variable utilisée dans pkgsrc.

{% highlight term %}
$ bmake -V INSTALL_PROGRAM
${INSTALL} ${COPY} ${_STRIPFLAG_INSTALL} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}

$ bmake show-var VARNAME=INSTALL_PROGRAM
/usr/bin/install -c -s -o rancune -g rancune -m 755
{% endhighlight %}

Avis personnel, je suis de plus en plus convaincu par pkgsrc. Je trouve le système particulièremenr efficace, pas vous ?

Revenons maintenant à notre initiation dans la voie du packageur :)

# Ecrire un patch :

Nous retournons maintenant à notre package, et à notre projet [truefalse v1.0](https://gitlab.com/iMil/truefalse/-/commits/v1.0). On recompile le projet, afin de revenir au même point que la semaine dernière :

{% highlight term %}
$ cd /home/rancune/pkgsrc/wip/truefalse
$ bmake install clean
{% endhighlight %}

Lorsque nous avions testé ce programme, nous nous étions rendus compte qu'il ne fonctionnait pas correctement : lancé sans argument, le programme renvoie false ! 

Et Pour cause :

{% highlight c %}

#include <stdlib.h> 

int
main(int argc, char *argv[]) 
{
    if (argc < 1) 		
        return EXIT_SUCCESS; 		

    return EXIT_FAILURE; 
}
{% endhighlight %}


On voit le bug : le test devrait être (arc<2) et non pas (argc<1) !

En tant que packagers, nous décidons de corriger le bug, et de proposer upstream (aux développeurs du projet original) un patch corrigeant cette petite erreur !

On commence donc par extraire les sources de truefalse :

{% highlight term %}
$ bmake extract
=> Bootstrap dependency digest>=20211023: found digest-20220214
=> Checksum BLAKE2s OK for truefalse-v1.0.tar.gz
=> Checksum SHA512 OK for truefalse-v1.0.tar.gz
===> Installing dependencies for truefalse-1.0
=> Tool dependency cwrappers>=20150314: found cwrappers-20220403
===> Checking for vulnerabilities in truefalse-1.0
===> Overriding tools for truefalse-1.0
===> Extracting for truefalse-1.0
{% endhighlight %}

Dans le meta-package pkg-developer que nous avons installé la dernière fois, il y a un outil qui claque : **pkgvi** ! 

{% highlight term %}
$ pkgvi work/truefalse-v1.0/truefalse.c 
pkgvi: File was modified. For a diff, type:
pkgdiff "work/truefalse-v1.0/truefalse.c"

$ ls work/truefalse-v1.0/
total 16
-rw-r--r-- 1 rancune rancune  46 22 avril 16:33 Makefile
-rw-r--r-- 1 rancune rancune  81 22 avril 16:33 README.md
-rw-r--r-- 1 rancune rancune 120  1 mai   12:24 truefalse.c
-rw-r--r-- 1 rancune rancune 120 22 avril 16:33 truefalse.c.orig

{% endhighlight %}

pkgvi ouvre une fenêtre vim pour éditer le code, et crée tout seul le truefalse.c.orig contenant le code avant modification. C'est pas le luxe ça ?

Pour créer le patch, on fait encore une fois appel à pkgsrc, et à la commande **mkpatches**

{% highlight term %}
$ mkpatches

$ ls
total 28
-rw-r--r--  1 rancune rancune    0  1 mai   12:23 '=20150314:'
-rw-r--r--  1 rancune rancune    0  1 mai   12:23 '=20211023:'
-rw-r--r--  1 rancune rancune    0  1 mai   12:23  Bootstrap
-rw-r--r--  1 rancune rancune    0  1 mai   12:23  Checking
-rw-r--r--  1 rancune rancune    0  1 mai   12:23  Checksum
-rw-r--r--  1 rancune rancune   81 25 avril 12:15  DESCR
-rw-r--r--  1 rancune rancune  312 25 avril 12:10  distinfo
-rw-r--r--  1 rancune rancune    0  1 mai   12:23  Extracting
-rw-r--r--  1 rancune rancune    0  1 mai   12:23  Installing
-rw-r--r--  1 rancune rancune  451 25 avril 12:37  Makefile
-rw-r--r--  1 rancune rancune  425 25 avril 12:10  Makefile.url2pkg~
-rw-r--r--  1 rancune rancune    0  1 mai   12:23  Overriding
drwxr-xr-x  2 rancune rancune 4096  1 mai   12:26  patches
-rw-r--r--  1 rancune rancune   32 25 avril 12:41  PLIST
-rw-r--r--  1 rancune rancune    0  1 mai   12:23  Tool
drwxr-xr-x 11 rancune rancune 4096  1 mai   12:23  work

{% endhighlight %}

Un nouveau repertoire, "patches", s'est créé. Il contient notre patch. Néanmoins ce nouveau fichier est absent de notre liste de fichiers contenue dans distinfo. Pour l'y ajouter, on utilise la cible makepatchsum, aussi abrégée mkps :


{% highlight term %}
$ bmake makepatchsum

$ cat distinfo
$NetBSD$

BLAKE2s (truefalse-v1.0.tar.gz) = 0ebf7a1aaef5f20a6c0df0a0cb38cc9ff7cf1d7d511e18c2ad5a4ec[...]
SHA512 (truefalse-v1.0.tar.gz) = 0be26cb93917249540e7d0d37805dcd4540382df91fafc9b9c48a15b[...]
Size (truefalse-v1.0.tar.gz) = 492 bytes

{% endhighlight %}

Notre nouvelle version du package, incluant le patch, est prête ! il ne reste plus qu'à la tester :


{% highlight term %}
$ bmake clean
===> Cleaning for truefalse-1.0

$ bmake update
===> Checking for vulnerabilities in truefalse-1.0
===> Deinstalling for truefalse-1.0
Running /home/rancune/pkg/sbin/pkg_delete -K /home/rancune/pkg/pkgdb -r truefalse-1.0
=> Bootstrap dependency digest>=20211023: found digest-20220214
=> Checksum BLAKE2s OK for truefalse-v1.0.tar.gz
=> Checksum SHA512 OK for truefalse-v1.0.tar.gz
===> Installing dependencies for truefalse-1.0
=> Tool dependency nbpatch-[0-9]*: found nbpatch-20151107
=> Tool dependency cwrappers>=20150314: found cwrappers-20220403
===> Checking for vulnerabilities in truefalse-1.0
===> Overriding tools for truefalse-1.0
===> Extracting for truefalse-1.0
===> Patching for truefalse-1.0
=> Applying pkgsrc patches for truefalse-1.0
=> Ignoring patchfile /home/rancune/pkgsrc/wip/truefalse/patches/patch-truefalse.c.orig
===> Creating toolchain wrappers for truefalse-1.0
===> Configuring for truefalse-1.0
===> Building for truefalse-1.0
cc -O2 -D_FORTIFY_SOURCE=2    -c truefalse.c
cc -Wl,-zrelro -Wl,-R/home/rancune/pkg/lib    -o truefalse truefalse.o
===> Installing for truefalse-1.0
=> Generating pre-install file lists
=> Creating installation directories
=> Automatic manual page handling
=> Generating post-install file lists
=> Checking file-check results for truefalse-1.0
=> Creating binary package /home/rancune/pkgsrc/wip/truefalse/work/.packages/truefalse-1.0.tgz
===> Building binary package for truefalse-1.0
=> Creating binary package /home/rancune/pkgsrc/packages/All/truefalse-1.0.tgz
===> Installing binary package of truefalse-1.0
===> Cleaning for truefalse-1.0
{% endhighlight %}


On voit bien que le patch est appliqué ! Et si l'on teste, notre programme fonctionne désormais !

Fiers comme des paons (et franchement, il y a de quoi non ?), nous pouvons soumettre notre patch upstream. Car, et c'est une partie importante du message d'aujourd'hui, le packageur joue un rôle important dans le développement d'une application en y participant activement. Il n'est d'ailleurs pas rare qu'un packageur "adopte" un projet laissé à l'abandon par ses développeurs, mais dont sa communauté a besoin !

Ce n'est pas sans joie que nous voyons apparaître une [version 1.1][https://gitlab.com/iMil/truefalse/-/commits/v1.1] de truefalse, intégrant notre patch ...   \o/


# Sortie de truefalse v1.1 ? On y va !

Puisque la version 1.1 est sortie, nous allons mettre à jour notre package. Je vous entends déjà : "Quoi ?? Faut tout refaire ?" Que nenni ! Avec pkgsrc, ceci va se faire sans douleur !

On commence donc pas modifier notre Makefile pour passer à la version 1.1. Quitte à y être, on en profite pour mettre le numéro de version dans une variable VERS, ça évitera les carabistouilles !


{% highlight Makefile %}
# $NetBSD$

VERS=           v1.1
DISTNAME=       truefalse-${VERS}
PKGNAME=        ${DISTNAME:S,-v,-,}
CATEGORIES=     misc
MASTER_SITES=   https://gitlab.com/iMil/truefalse/-/archive/${VERS}/

MAINTAINER=     truefalse@rancune.org
HOMEPAGE=       https://gitlab.com/iMil/truefalse/-/archive/${VERS}/
COMMENT=        First attempt at making a package
LICENSE=        original-bsd

INSTALLATION_DIRS=      bin

do-install:
	${INSTALL_PROGRAM} ${WRKSRC}/${PROGNAME} ${DESTDIR}/${PREFIX}/bin/${PROGNAME}

.include "../../mk/bsd.pkg.mk"
{% endhighlight %}

Désormais, ce sont bien les sources de la version 1.1 qui seront utilisées :

{% highlight term %}
$ bmake fetch
=> Bootstrap dependency digest>=20211023: found digest-20220214
=> Fetching truefalse-v1.1.tar.gz
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   487  100   487    0     0   1497      0 --:--:-- --:--:-- --:--:--  1503
{% endhighlight %}

On remet à jour le fichier distinfo, afin que les checksums de cette nouvelle version y soient intégrés :

{% highlight term %}
$ bmake makesum
=> Bootstrap dependency digest>=20211023: found digest-20220214

$ cat distinfo
$NetBSD$

BLAKE2s (truefalse-v1.1.tar.gz) = 2f3f23cb1606ab1cec7e856db861d4d383c482273108307ae41516e[...]
SHA512 (truefalse-v1.1.tar.gz) = 4a8cad86ea98452e5c7f2a669d61c92447a9337aca8d444e2c261a6c[...]
Size (truefalse-v1.1.tar.gz) = 487 bytes
SHA1 (patch-truefalse.c) = 78e58a136994df49789451bfa5f667b183dfd80c

{% endhighlight %}

Nous allons également pouvoir retirer notre patch du package, puisqu'il a été intégré upstream. Il ne faut alors pas oublier de retirer également les checkums des patchs du fichier distinfo :


{% highlight term %}
$ rm -fr patches
$ bmake makepatchsum
{% endhighlight %}

On teste ... et ça fonctionne !!!

{% highlight term %}
$ bmake update clean
$ truefalse
{% endhighlight %}

# You have new mail

Une nouvelle journée s'offre à vous : les oiseaux chantent, le soleil brille, ça sent bon le café dans le bureau ... Et vous recevez un nouveau mail : OOOOOOh, une version 2.0 de true false est sortie !

Vous jetez un coup d'oeil aux sources, et là c'est le drame :


{% highlight c %}
#include <stdlib.h>
#include <glib.h>

int
main(int argc, char *argv[])
{
	if (argc < 2) {
		g_assert_true(argc < 2);

		return EXIT_SUCCESS;
	}

	g_assert_false(argc < 2);

	return EXIT_FAILURE;
}
{% endhighlight %}

Mais ... mais ... mais ???? Pourquoi ??? Notre développeur a décidé d'utiliser la glib ?????

Et ce Makefile, là : pourquoi abandonner bmake ? On utilise la version Gnu de make maintenant ????

{% highlight Makefile %}

CFLAGS=  $(shell pkg-config --cflags glib-2.0)
LDFLAGS= $(shell pkg-config --libs glib-2.0)

truefalse: truefalse.c
	$(CC) $(CFLAGS) $< -o $@ $(LDFLAGS)
{% endhighlight %}


Il va falloir gérer tout ça ... Mais ce sera dans la prochaine vidéo !

A très bientôt,

Rancune.

# Bibliographie


  * [ La vidéo sur laquelle se basent ces notes](https://www.youtube.com/watch?v=ilR6ThLAqJ4) 

 * [https://wiki.netbsd.org/pkgsrc/intro_to_packaging/](https://wiki.netbsd.org/pkgsrc/intro_to_packaging/)
